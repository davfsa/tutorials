# discord.py tutorial

***by `Tmpod#2525`***

---

> __NOTICE:__ On the 28th of August 2021, discord.py has been [officially discontinued](https://gist.github.com/Rapptz/4a2f62751b9600a31a0d3c78100287f1).
> You are advised to use [Hikari](https://github.com/hikari-py/hikari). You could also use one of discord.py's maintained forks.

---

Welcome to this tutorial about the popular Python library `discord.py`. As of the writing of this tutorial, discord.py is the only properly usable wrapper for Python. It uses `asyncio` to achieve concurrency and so, it may be confusing to new programmers. That's why I'm here. However, it is strongly advised that you get some basic knowledge about Python first, including a good notion of what decorators are and what the [Object Oriented Programming paradigm](https://realpython.com/python3-object-oriented-programming/) is.

In this tutorial I'll cover everything from just the base client to the more complex command framework. Just grab your text editor and a good snack and come along with me in this tutorial.

<!-- TODO: Add challenges -->

Whenever you are ready just proceed onto [**Getting Started**](./getting-started.html)!
