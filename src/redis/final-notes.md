# Final Notes

Welcome to the last section of this tutorial. This is where I gather some random stuff I find interesting to know. I hope you enjoy!

**Summary:**

- [Hiding secret arguments](#how-to-hide-your-redis-pool-arguments-for-aioredis)
- [Custom command prefixes](#how-to-use-redis-to-make-custom-guild-prefixes)


## How to hide your Redis pool arguments for aioredis
In this little guide I'll show you how to use dicts to store arguments.
As you *should* know, Python functions can take positional arguments (usually referred as *args*) and keyword arguments (usually referred as *kwargs*). Fortunately, Python has a very neat feature. You can pass a starred variable (expanded variable) that holds arguments for functions. For positional ones you use a list and for kwargs you use dicts.  
With that said, what I advise you to do is to use a JSON file or other data serialization filetype to store "start-up arguments" (stuff that you need to start your app, like the bot token, database access info, default prefixes, etc). If you store your token directly in your code, **please** change it now to an external file or environment variable.

So let's imagine you have the following JSON file where you store your token and some other data.
```json
{
  "token": "4sDF-M0oV1eS-4rE-awE5oM3", 
  "prefix": "foo!",
  "bar": true
}
```

So how would you put your kwargs in there? Like this, for example
```json
{
  "token": "4sDF-M0oV1eS-4rE-awE5oM3", 
  "prefix": "foo!",
  "bar": true
  "redis-config": {
    "address": "redis://your.address.or.localhost",
    "password": "asdfmoviesarebae", 
    "encoding": "utf-8"
  } 
}
```

And how to load that data I hear you asking? Fear not! I'll tell you right away.
```py
...
# before the bot definition 
with open('json/file/path.json') as fp:
    json_data = json.load(fp) # you got to import json of course
```

And going back to the [**Starting with `aioredis`**](./starting-with-aioredis.html), here's how you'd expand the Redis arguments
```py
# in the 'on_start' event
db_args = json_data['redis-config']
db_pool = await aioredis.create_redis_pool(**db_args)
```

And that's all for this mini guide!

---

## How to use redis to make custom guild prefixes
So this guide will teach you how to use Redis to make custom guild prefixes. The code I'll show here will be very simple and a bit feature limited. It's on purpuse since I just want to teach you the basics and not giving you the full stuff. *I'm only doing this because making custom guild prefixes was the main reason I started using a database and chose Redis*

Steps to achieving a basic custom prefix system:

- Setup a default prefix(es)
- Create a function to get the prefix out of the redis db
- Create a command to change it  
 
Sounds simple right? That's beause it is. 😉

So, for the default prefix I have some recommendations to do.
First, do not choose any common chars, like`!`, `?`, `-`, etc. Make it original. People will be able to set it to a more comfy and easy prefix if they want.
Second, don't make it too long for obvious reasons. `cyborgtoast!` isn't a good default prefix, whereas `cb!` or `t.` is a good one. Try aiming for something with the following structure: `<2 to 3 chars relating to your bot's name><"seperation char" like ".", "!", "?" or "-">`.  
Where to store it? In your config file, next to the token.

Next up - creating a function to get the prefix for the guild or return the default one if none is found
```py
# this function will be defined before the 'bot' var and it will take a 'bot' and a 'message' argument
async def get_guild_prefix(bot, message):
    # going to use hashes to save the prefix because you might want to store more config vars
    prefix = await bot.db_pool.hget(f"guilds:{message.guild.id}", "prefix")
    if prefix and message.guild: # seeing  if there's a registered custom prefix and if the command is not being executed in a DM
        return prefix # return it if so
    return bot.default_prefix # else return the default one
```

You can also one-line it
```py
async def get_guild_prefix(bot, message):
    return await bot.db_pool.hget(f"guilds:{message.guild.id}", "prefix") if await bot.db_pool.hget(f"guilds:{message.guild.id}", "prefix") and message.guild else bot.default_prefix
```
The thing is, with that code you fetch the values two times
Or else if you really want to one-line it and want a bit more of efficiency you can set the prefix to the default one upon guild join. So everything would be:
```py
async def get_guild_prefix(bot, message):
    return await bot.db_pool.hget(f"guilds:{message.guild.id}", "prefix") if message.guild else bot.default_prefix
```
and then add the listener
```py
@bot.listen()
async def on_guild_join(guild):
    await bot.db_pool.hset(f"guilds:{guild.id}", "prefix", bot.default_prefix)
```

Then we got to give the function in the `bot` assignement
```py
bot = libneko.Bot(command_prefix=get_guild_prefix, ...) # pass the function without the brackets
```

Now, for the last part we need a command that will change the prefix. Easy peasy.
```py
@bot.command()
async def changeprefix(ctx, prefix: str): # using type annotation but it's not required
    """Change the guild's registered command prefix"""
    if len(prefix) >= 5: # setting a max length
        return await ctx.send("That prefix is too long")

    await bot.db_pool.hset(f"guilds:{ctx.guild.id}", "prefix", prefix)

    await ctx.send("Successfully changed this guild's prefix")
```
This is **very** simple of course because I just want to give you the basics.
**Note:** As you saw I was using `:` as you would use `/` in a path, for example. This is just they way I like to categorise things in Redis.

That's all for this mini guide!





